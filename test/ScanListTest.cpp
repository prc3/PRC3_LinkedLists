#include "gtest/gtest.h"
#include "ScanList.h"

TEST(TestScanList, test_addscan_right_location)
{
    ScanList scanList;

    for(int i = 0; i < 10; i++)
    {
        if(i != 5) scanList.addScan(i);
    }

    scanList.addScan(5);

    Scan* locationFive = scanList.getScanByNr(5);
    ASSERT_TRUE(locationFive != __null);

    EXPECT_EQ(5, locationFive->getSerialNumber());
}

TEST(TestScanList, test_addscan_duplicate)
{
    ScanList scanList;

    for(int i = 0; i < 10; i++)
    {
        scanList.addScan(i);
    }

    EXPECT_EQ(0, scanList.getScanByNr(0)->getTimesRecycled());
    EXPECT_EQ(0, scanList.getScanByNr(1)->getTimesRecycled());

    scanList.addScan(1);

    EXPECT_EQ(1, scanList.getScanByNr(1)->getTimesRecycled());

    scanList.addScan(1);

    EXPECT_EQ(2, scanList.getScanByNr(1)->getTimesRecycled());

}

TEST(TestScanList, test_removescan)
{
    ScanList scanList;

    for(int i = 0; i < 10; i++)
    {
        scanList.addScan(i);
    }

    bool result = scanList.removeScan(3);
    ASSERT_TRUE(result);

    Scan* occupiedSpace = scanList.getScanByNr(3);
    ASSERT_TRUE(occupiedSpace != __null);

    EXPECT_EQ(4, occupiedSpace->getSerialNumber());

}

TEST(TestScanList, test_removescan_nonexisting)
{
    ScanList scanList;

    for(int i = 0; i < 10; i++)
    {
        scanList.addScan(i);
    }

    bool result = scanList.removeScan(666);
    EXPECT_FALSE(result);

}

TEST(TestScanList, test_getscanbynr_invalid)
{
    ScanList scanList;
    ASSERT_TRUE(scanList.getScanByNr(435) == __null);
}

TEST(TestScanList, test_scan_sorting)
{

    ScanList scanList;
    scanList.addScan(53);
    scanList.addScan(2);
    scanList.addScan(23423);
    scanList.addScan(1);

    ASSERT_TRUE(scanList.getScanByNr(0) != __null);
    ASSERT_TRUE(scanList.getScanByNr(1) != __null);
    ASSERT_TRUE(scanList.getScanByNr(2) != __null);
    ASSERT_TRUE(scanList.getScanByNr(3) != __null);

    EXPECT_EQ(1, scanList.getScanByNr(0)->getSerialNumber());
    EXPECT_EQ(2, scanList.getScanByNr(1)->getSerialNumber());
    EXPECT_EQ(53, scanList.getScanByNr(2)->getSerialNumber());
    EXPECT_EQ(23423, scanList.getScanByNr(3)->getSerialNumber());

}

TEST(TestScanList, test_times_recycled)
{
    ScanList scanList;
    scanList.addScan(3);
    EXPECT_EQ(0, scanList.getTimesRecycled(3));
    scanList.addScan(3);
    EXPECT_EQ(1, scanList.getTimesRecycled(3));
    scanList.addScan(3);
    EXPECT_EQ(2, scanList.getTimesRecycled(3));
}

TEST(TestScanList, test_times_recycled_invalid)
{
    ScanList scanList;
    EXPECT_EQ(0, scanList.getTimesRecycled(34));
}