#include "gtest/gtest.h"
#include "Scan.h"

TEST(TestScan, test_constructor_values)
{
    Scan* scan = new Scan(1);
    EXPECT_EQ(1, scan->getSerialNumber());
    EXPECT_EQ(0, scan->getTimesRecycled());
    delete scan;
}

TEST(TestScan, test_invalid_constructor_values)
{
    EXPECT_THROW(Scan(-1), std::out_of_range);
}

TEST(TestScan, test_times_recycled)
{
    Scan* scan = new Scan(1);
    scan->recycle();
    EXPECT_EQ(1, scan->getTimesRecycled());
    scan->recycle();
    scan->recycle();
    EXPECT_EQ(3, scan->getTimesRecycled());
    delete scan;
}

TEST(TestScan, test_set_next_value)
{
    Scan* scan = new Scan(1);
    Scan* scan2 = new Scan(2);
    scan->setNext(scan2);
    EXPECT_EQ(scan2, scan->getNext());
    delete scan;
    delete scan2;
}
/*
TEST(TestScan, test_set_next_value_self)
{
    Scan* scan = new Scan(1);
    EXPECT_THROW(scan->setNext(scan), std::invalid_argument);
}
*/
