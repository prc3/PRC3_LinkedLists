#include "Scan.h"
#include <stdexcept>

Scan::Scan(int number) : serialNumber(number), next(__null){
    if(number < 0) throw std::out_of_range("Serial number can't be negative.");
    timesRecycled = 0;
}

Scan::~Scan() {

}


int Scan::getSerialNumber() const {
    return serialNumber;
}


Scan* Scan::getNext() {
    return next;
}

void  Scan::setNext(Scan* nextScan) {
//    if(nextScan == this) throw std::invalid_argument("Can't point to self.");
    next = nextScan;
}

void Scan::recycle(){
    timesRecycled++;
}

int Scan::getTimesRecycled() const {
    return timesRecycled;
}

