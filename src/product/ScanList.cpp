#include "ScanList.h"

ScanList::ScanList() : head(__null) {

}

ScanList::~ScanList() {
    Scan* headOfList = head;
    while(headOfList != __null)
    {
        delete headOfList;
        headOfList = headOfList->getNext();
    }
}

void ScanList::addScan(int serialNumber) {
    if(serialNumber < 0) return;

    Scan* scanPointer = head;
    bool alreadyInList = false;

    while(scanPointer != __null)
    {
        if(scanPointer->getSerialNumber() == serialNumber)
        {
            alreadyInList = true;
            break;
        }
        scanPointer = scanPointer->getNext();
    }

    if(!alreadyInList) // Create a new scan
    {
        Scan* newScan = new Scan(serialNumber);

        if(head == __null) // If it is the first item...
        {
            head = newScan;
        }
        else
        {
            if(serialNumber < head->getSerialNumber())
            {
                newScan->setNext(head);
                head = newScan;
            }
            else
            {
                bool isLastItem = true;
                Scan* scanSortPointer = head->getNext();
                Scan* scanSortPreviousPointer = head;

                while(scanSortPointer != __null)
                {
                    if(serialNumber > scanSortPreviousPointer->getSerialNumber() && serialNumber < scanSortPointer->getSerialNumber())
                    {
                        isLastItem = false;

                        scanSortPreviousPointer->setNext(newScan);
                        newScan->setNext(scanSortPointer);

                        break;
                    }
                    scanSortPreviousPointer = scanSortPointer;
                    scanSortPointer = scanSortPointer->getNext();
                }

                if(isLastItem) // If it turns out it is the last item...
                {
                    scanSortPreviousPointer->setNext(newScan);
                }
            }
        }

    }
    else
    {
        scanPointer->recycle();
    }
}

Scan* ScanList::getScanByNr(int position) {
    if(position < 0 || head == __null) return __null;

    Scan* scanPointer = head;
    int i = 0;
    while(i < position && scanPointer != __null)
    {
        i++;
        scanPointer = scanPointer->getNext();
    }

    return scanPointer;
}

bool ScanList::removeScan(int serialNumber) {
    if(serialNumber < 0 || head == __null) return false;

    Scan* scanPointer = head;
    Scan* previousScanPointer = __null;

    while(scanPointer != __null)
    {
        if(scanPointer->getSerialNumber() == serialNumber)
        {
            break;
        }

        previousScanPointer = scanPointer;
        scanPointer = scanPointer->getNext();
    }

    if(scanPointer == head && scanPointer != __null)
    {
        head = scanPointer->getNext();
        delete scanPointer;
        return true;
    }
    else if(scanPointer == __null)
    {
        return false;
    }
    else
    {
        previousScanPointer->setNext(scanPointer->getNext());
        delete scanPointer;
        return true;
    }
}

int ScanList::getTimesRecycled(int serialNumber) {
    if(serialNumber < 0 || head == __null) return 0;

    Scan* scanPointer = head;
    while(scanPointer != __null)
    {
        if(scanPointer->getSerialNumber() == serialNumber)
        {
            break;
        }
        scanPointer = scanPointer->getNext();
    }

    if(scanPointer == __null) return 0;
    return scanPointer->getTimesRecycled();
}