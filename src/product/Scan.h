#ifndef PRC3_LINKEDLISTS_SCAN_H
#define PRC3_LINKEDLISTS_SCAN_H


class Scan {
private:
    int serialNumber;
    int timesRecycled;
    Scan* next;

public:
    // pre: -
    // post: serialNumber == number and timesRecycled == 0
    Scan(int number);

    // pre: -
    // post: object has been destructed
    virtual ~Scan();

    // pre: -
    // post: serialNumber has been returned
    int getSerialNumber() const;

    // pre: -
    // post: next scan has been returned
    Scan* getNext();

    // pre: -
    // post: next points to nextScan
    void setNext(Scan* nextScan);

    // pre: -
    // post: timesRecycled has been increased by one
    void recycle();

    // pre: -
    // post: timesRecycled has been returned
    int getTimesRecycled() const;
};


#endif //PRC3_LINKEDLISTS_SCAN_H
